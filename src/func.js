const getSum = (str1, str2) => {
  if(typeof(str1)=="object"||typeof(str2)=="object"||Array.isArray(str1)||Array.isArray(str2))
    return false;
  var n1 = str1==""?0:parseInt(str1);
  var n2 = str2==""?0:parseInt(str2);
  if(isNaN(n1)||isNaN(n2))
    return false;
  return (n1+n2).toString();  
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0;
  var comments1 = 0;
  for(var post of listOfPosts)
  {
    if(post.author==authorName)
    {
      posts++;
    }
    if(Array.isArray(post.comments))
    {
      for(var comment of post.comments)
      {
        if(comment.author==authorName)
        {
          comments1++;
        }
      }
    }
  }
  return "Post:"+posts.toString()+",comments:"+comments1.toString();
};

const tickets=(people)=> {
  var amount25 = 0;
  var amount50 = 0;
  for(var person of people)
  {
    if(person-25!=0)
    {
      if(person==50)
        amount25--;
      else
      {
        if(amount25-3<0)
        {
          amount50--;
          amount25--;
        }
        else
          amount25-=3;
      }
    }
    if(amount25<0||amount50<0)
      return 'NO'
    if(person==25)
      amount25++;
    if(person==50)
      amount50++;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
